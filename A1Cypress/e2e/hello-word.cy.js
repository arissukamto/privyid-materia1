/// <reference types="cypress" />


describe('Basic desktop Tests', () => {
    it('The webpage loads, at least', () => {
      cy.visit('https://codedamn.com')
    })

    it ('Login page looks good', () => { 

      cy.viewport(1280, 720)
      cy.visit('https://codedamn.com')

      cy.contains('Sign in').click({force: true})
      
      

    })

    it('the login page links work', () =>{

      cy.viewport(1280, 720)
      cy.visit('https://codedamn.com')

      cy.contains('Sign in').click({force: true})

      cy.contains('Forgot your password?').click({force: true})

      cy.url().should('include','/password-reset')

      cy.url().then(value => {
          cy.log('Current URL = ', cy.url ())
      })

      cy.go('back')
      cy.contains('Create one').click({force: true})

      cy.url().should('include','/register')

  })


    it('Login Should display correct error',() =>{

      cy.viewport(1280, 720)
      cy.visit('https://codedamn.com')
      

      cy.contains('Sign in').click({force: true})
      cy.contains('Sign in to codedamn').should('exist')
      cy.contains('Unable to authorize').should('not.exist')
      

      cy.get('[data-testid=username]').type('admin', {force: true})
      cy.get('[data-testid=password]').type('admin', {force: true})
      cy.get('[data-testid=login]').click({force: true})

      cy.contains('Unable to authorize').should('not.exist')

  })

    it('Login should work fine',() =>{

    cy.viewport(1280, 720)
      cy.visit('https://codedamn.com')

    cy.contains('Sign in').click({force: true})
    

    cy.get('[data-testid=username]').type('iosdev', {force: true})
    cy.get('[data-testid=password]').type('iosdev', {force: true})
    cy.get('[data-testid=login]').click({force: true})

    // cy.url().should('include', '/dashboard')  
  })




})


